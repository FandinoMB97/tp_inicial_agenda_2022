/*Drop DATABASE IF EXIST `agenda`;
CREATE DATABASE `agenda`;
USE agenda;
CREATE TABLE `personas`
(
  `idPersona` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(50) NOT NULL,
  `Telefono` varchar(50) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `FechaDeNacimiento` varchar(50) NOT NULL,
  `TipoDeContacto` varchar(50) NOT NULL,
  `Provincia` varchar(50) NOT NULL,
  `Localidad` varchar(50) NOT NULL,
  `Calle` varchar(50) NOT NULL,
  `Altura` varchar(50) NOT NULL,
  PRIMARY KEY (`idPersona`)
);
/*---------------------------------------------- Anterior*/
DROP DATABASE IF EXIST `agenda`;
CREATE DATABASE `agenda`;
USE agenda;
CREATE TABLE tablename (
   `colname` SERIAL
);
CREATE TABLE `personas`
(
  `idPersona` SERIAL PRIMARY KEY,
  `Nombre` varchar(50) NOT NULL,
  `Telefono` varchar(50) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `FechaDeNacimiento` varchar(50) NOT NULL,
  `TipoDeContacto` varchar(50) NOT NULL,
  `Provincia` varchar(50) NOT NULL,
  `Localidad` varchar(50) NOT NULL,
  `Calle` varchar(50) NOT NULL,
  `Altura` varchar(50) NOT NULL,
  `GrupoMusical` varchar(50) NOT NULL,
  `SignoZ` varchar(50) NOT NULL
);
/*----------------------------------------------Lo que uso
CREATE DATABASE agenda;

DROP TABLE IF EXISTS personas;
CREATE TABLE tablename (
   colname SERIAL
);
CREATE TABLE personas
(
  idPersona SERIAL PRIMARY KEY,
  Nombre varchar(50) NOT NULL,
  Telefono varchar(50) NOT NULL,
  Email varchar(50) NOT NULL,
  FechaDeNacimiento varchar(50) NOT NULL,
  TipoDeContacto varchar(50) NOT NULL,
  Provincia varchar(50) NOT NULL,
  Localidad varchar(50) NOT NULL,
  Calle varchar(50) NOT NULL,
  Altura varchar(50) NOT NULL,
  GrupoMusical varchar(50) NOT NULL,
  SignoZ varchar(50) NOT NULL
); 
* 
* 
CREATE TABLE colores
(
  id varchar(50) NOT NULL,
  color varchar(50) NOT NULL
);
insert into colores(id, color)
values('1','azul'),
values('2','verde'),
values('3','rojo'),
values('4','azul'),
values('5','azul'),
values('6','rojo');
* 
* 
* 
* 
*/
*/
------------------------------------------------*/