package dto;

public class PersonaDTO 
{
	private int _idPersona;
	private String _nombre;
	private String _telefono;

	//nuevos
	private String _email;
	private String _fecha;
	private String _tipo;
	private String _provincia;
	private String _locaclidad;
	private String _calle;
	private String _altura;
	
	//agregados de catedra
	private String _grupoMusical;
	private String _signoZ;

	public PersonaDTO(int idPersona, String nombre, String telefono, String email, String fecha, String tipo, String provincia, String localidad, String calle, String altura, String gmusical, String signoz)
	{
		_idPersona = idPersona;
		_nombre = nombre;
		_telefono = telefono;
		_email= email;    
		_fecha= fecha;    
		_tipo= tipo;
		_provincia= provincia;     
		_locaclidad= localidad;     
		_calle= calle;    
		_altura= altura;
		_grupoMusical = gmusical;
		_signoZ = signoz;
	}
	
	public int getIdPersona() 
	{
		return _idPersona;
	}
	public void setIdPersona(int idPersona) 
	{
		_idPersona = idPersona;
	}

	public String getNombre() 
	{
		return _nombre;
	}
	public void setNombre(String nombre) 
	{
		_nombre = nombre;
	}

	public String getTelefono() 
	{
		return _telefono;
	}
	public void setTelefono(String telefono) 
	{
		_telefono = telefono;
	}

	public String getEmail()
	{
		return _email;
	}
	public void setEmail(String email)
	{
		_email = email;
	}

	public String getFecha()
	{
		return _fecha;
	}
	public void setFecha(String fecha)
	{
		_fecha = fecha;
	}

	public String getTipo()
	{
		return _tipo;
	}
	public void setTipo(String tipo)
	{
		_tipo = tipo;
	}

	public String getProvincia()
	{
		return _provincia;
	}
	public void setProvincia(String provincia)
	{
		_provincia = provincia;
	}

	public String getLocalidad()
	{
		return _locaclidad;
	}
	public void setLocalidad(String localidad)
	{
		_locaclidad = localidad;
	}
	
	public String getCalle()
	{
		return _calle;
	}
	public void setCalle(String calle)
	{
		_calle = calle;
	}

	public String getAltura()
	{
		return _altura;
	}
	public void setAltura(String altura)
	{
		_altura = altura;
	}
	public String getGrupoMusical()
	{
		return _grupoMusical;
	}
	public void setGrupoMusical(String gmusical)
	{
		_grupoMusical = gmusical;
	}
	public String getSignoZ()
	{
		return _signoZ;
	}
	public void setSignoZ(String signoz)
	{
		_signoZ = signoz;
	}
}
