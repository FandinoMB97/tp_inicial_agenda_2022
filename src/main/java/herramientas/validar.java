package herramientas;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class validar {
	
	private Pattern patronEmail;
	private Pattern patronTelArg;

	public validar()
	{
		patronEmail = Pattern.compile
						( "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"+
						  "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
		patronTelArg = Pattern.compile
				("^(?:(?:00)?549?)?0?(?:11|[2368]\\d)(?:(?=\\d{0,2}15)\\d{2})??\\d{8}$");
	}
	/**
	 * @tipo formato a validar: ("email", "telefono", "fecha")*/
	public boolean check(String entrada, String tipo)
	{
		boolean ret = false;
		Pattern p;
		switch(tipo)
		{
		   case "email" :
		      p = patronEmail;
		      break;
		   
		   case "telefono" :
			  p = patronTelArg;
		      break;
		   
		   case "fecha" :
			   p = null;
			   break;
			   
		   default : 
			   p = null;
		}
		if (p != null)
		{
			Matcher validado = p.matcher(entrada);
			if (validado.find() == true)
				ret = true;
		}
		return ret;
	}
}
