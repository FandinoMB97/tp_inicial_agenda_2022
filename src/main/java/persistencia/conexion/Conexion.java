//package persistencia.conexion;
//
//import java.sql.Connection;
//import java.sql.DriverManager;
//import java.sql.SQLException;
//import org.apache.log4j.Logger;
//
//public class Conexion 
//{
//	public static Conexion instancia;
//	private Connection connection;
//	private Logger log = Logger.getLogger(Conexion.class);	
//	
//	private Conexion()
//	{
//		try
//		{
//			Class.forName("com.mysql.jdbc.Driver"); // quitar si no es necesario
//			this.connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/agenda","root","3468Maria");
//			this.connection.setAutoCommit(false);
//			log.info("Conexión exitosa");
//		}
//		catch(Exception e)
//		{
//			log.error("Conexión fallida", e);
//		}
//	}
//	
//	
//	public static Conexion getConexion()   
//	{								
//		if(instancia == null)
//		{
//			instancia = new Conexion();
//		}
//		return instancia;
//	}
//
//	public Connection getSQLConexion() 
//	{
//		return this.connection;
//	}
//	
//	public void cerrarConexion()
//	{
//		try 
//		{
//			this.connection.close();
//			log.info("Conexion cerrada");
//		}
//		catch (SQLException e) 
//		{
//			log.error("Error al cerrar la conexión!", e);
//		}
//		instancia = null;
//	}
//}
package persistencia.conexion;
import java.sql.*;

public class Conexion {
	
	public static Conexion instancia;
	private Connection BasedeDatos;
	
	private Conexion()
	{
		BasedeDatos = null;
//	    Statement st = null;
		try
		{    
		    this.BasedeDatos = DriverManager.getConnection("jdbc:postgresql://localhost:5432/agenda", "postgres", "root");
		    this.BasedeDatos.setAutoCommit(false);
		    System.out.println("conexion exitosa");
		    
//	        // Se hara una consulta  de la tabla y se mandara a imprimir.
//	        st = BaseDatos.createStatement();
//	        ResultSet rs = st.executeQuery( ""
//		            + "SELECT \"nombre\" "
//		            + "FROM \"persona\" ");
//	  
//	        while    ( rs.next() ) {
//	            String  n= rs.getString("nombre");
//	            System.out.println(n);
//	        }
//	        rs.close();
//	        st.close();
//	        BaseDatos.close();
		}
		 catch (Exception e)
		{
		        System.err.println( e.getMessage() );
		}
	}
	
	public static Conexion getConexion()   
	{								
		if(instancia == null)
		{
			instancia = new Conexion();
		}
		return instancia;
	}

	public Connection getSQLConexion() 
	{
		return this.BasedeDatos;
	}

	public void cerrarConexion()
	{
		try 
		{
			this.BasedeDatos.close();
			System.out.println("conexion cerrada");
		}
		catch (SQLException e) 
		{
			System.err.println( e.getMessage());
		}
		instancia = null;
	}
	
}

