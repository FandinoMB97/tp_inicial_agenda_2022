package presentacion.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import modelo.Agenda;
import presentacion.reportes.ReporteAgenda;
import presentacion.vista.Editar;
import presentacion.vista.VentanaError;
import presentacion.vista.VentanaPersona;
import presentacion.vista.Vista;
import dto.PersonaDTO;
import herramientas.validar;

public class Controlador implements ActionListener
{
		private Vista vista;
		private List<PersonaDTO> personasEnTabla;
		private VentanaPersona ventanaPersona;
		private Editar ventanaEditar;
		private Agenda agenda;
		private validar validador; 
		
		public Controlador(Vista vista, Agenda agenda)
		{
			this.vista = vista;
			this.vista.getBtnAgregar().addActionListener(a->ventanaAgregarPersona(a));
			this.vista.getBtnBorrar().addActionListener(s->borrarPersona(s));
			this.vista.getBtnReporte().addActionListener(r->mostrarReporte(r));
			this.vista.getBtnEditar().addActionListener(d->editarPersona(d));
			
			this.ventanaPersona = VentanaPersona.getInstance();
			this.ventanaPersona.getBtnAgregarPersona().addActionListener(p->guardarPersona(p));

			this.ventanaEditar = Editar.getInstance();
			this.ventanaEditar.getBtnGuardar().addActionListener(g->guardarEdicion(g));
			this.ventanaEditar.getBtnCancelar().addActionListener(c->cancelarEdicion(c));
			
			this.agenda = agenda;
			validador = new validar();
		}
		
		private void ventanaAgregarPersona(ActionEvent a)
		{
			this.ventanaPersona.mostrarVentana();
		}

		private void guardarPersona(ActionEvent p)
		{
			String nombre = this.ventanaPersona.getTxtNombre().getText();
			String telefono = this.ventanaPersona.getTxtTelefono().getText();
			String email = this.ventanaPersona.getTxtEmail().getText();
			String calle = this.ventanaPersona.getTxtCalle().getText();
			String altura = this.ventanaPersona.getTxtAltura().getText();
			String fecha;
			
			try
			{
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		        fecha = simpleDateFormat.format(this.ventanaPersona.getFecha().getDate());    
			}
			catch(Exception  e)
			{
				fecha = "---";
			}
			String tipo = (String) this.ventanaPersona.getTipo().getSelectedItem();
			String provincia = (String) this.ventanaPersona.getProvincia().getSelectedItem();
			String localidad = (String) this.ventanaPersona.getLocalidad().getSelectedItem();
			String gmusical = (String) this.ventanaPersona.getGrupoMusical().getSelectedItem();
			String signoz = (String) this.ventanaPersona.getSignoZ().getSelectedItem();
			
			if( tipo != null && provincia != null && localidad != null && !tipo.equals("---")
			  && !provincia.equals("---") && !localidad.equals("---") && !fecha.equals("---"))
			{
				boolean bEmail = validador.check(email, "email"); 
				boolean bTelefono = validador.check(telefono, "telefono"); 
				if (bEmail && bTelefono)
				{
					ArrayList<Integer> idUsados = new ArrayList<Integer>();
					for (PersonaDTO persona : personasEnTabla)
						idUsados.add(persona.getIdPersona());
					int idNuevo = 0;
					while(idUsados.contains(idNuevo))
						idNuevo++;
					PersonaDTO nuevaPersona = new PersonaDTO(idNuevo, nombre, telefono, email, fecha, tipo, provincia, localidad, calle, altura, gmusical, signoz);
					this.agenda.agregarPersona(nuevaPersona);
				}
				else
				{
					VentanaError noGuardado = new VentanaError();
					noGuardado.setTextoError((bEmail?"":"Error Email")+(bTelefono?"":"ErrorTelefono"));
					noGuardado.setVisible(true);
					noGuardado.setLocationRelativeTo(null);
				}
			}
			else
			{
				VentanaError noGuardado = new VentanaError();
				noGuardado.setTextoError("Datos Incompletos");
				noGuardado.setVisible(true);
				noGuardado.setLocationRelativeTo(null);
			}
			this.refrescarTabla();
			this.ventanaPersona.cerrar();
		}

		private void mostrarReporte(ActionEvent r)
		{
			String texto = (String) vista.getReporteBox().getSelectedItem();
			ReporteAgenda reporte = new ReporteAgenda(texto);
			reporte.mostrar();	
		}

		public void borrarPersona(ActionEvent s)
		{
			int[] filasSeleccionadas = this.vista.getTablaPersonas().getSelectedRows();
			for (int fila : filasSeleccionadas)
			{
				this.agenda.borrarPersona(this.personasEnTabla.get(fila));
			}
			
			this.refrescarTabla();
		}
		
		public void editarPersona(ActionEvent s)
		{
			ventanaEditar.vaciar();
			int[] filasSeleccionadas1 = this.vista.getTablaPersonas().getSelectedRows();
			int[] filasSeleccionadas2 = this.vista.getTablaPersonas().getSelectedColumns();
			if (filasSeleccionadas1.length > 0 && filasSeleccionadas2.length > 0)
			{
				int fila = filasSeleccionadas1[filasSeleccionadas1.length -1];
				int colu = filasSeleccionadas2[filasSeleccionadas2.length -1];
				
				PersonaDTO seleccionado = this.personasEnTabla.get(fila);
				
				switch(colu)
				{
					case 0:
						ventanaEditar.editarNombre(seleccionado.getNombre(), fila);
						ventanaEditar.setVisible(true);
						ventanaEditar.setLocationRelativeTo(null);
						break;
					case 1:
						ventanaEditar.editarTelefono(seleccionado.getTelefono(), fila);
						ventanaEditar.setVisible(true);
						ventanaEditar.setLocationRelativeTo(null);
						break;
					case 2:
						ventanaEditar.editarEmail(seleccionado.getEmail(), fila);
						ventanaEditar.setVisible(true);
						ventanaEditar.setLocationRelativeTo(null);
						break;
					case 3:
						ventanaEditar.editarFecha(seleccionado.getFecha(), fila);
						ventanaEditar.setVisible(true);
						ventanaEditar.setLocationRelativeTo(null);
						break;
					case 4:
						ventanaEditar.editarTipo(seleccionado.getTipo(), fila);
						ventanaEditar.setVisible(true);
						ventanaEditar.setLocationRelativeTo(null);
						break;
					case 5:
						ventanaEditar.editarProvincia(seleccionado.getProvincia(), fila);
						ventanaEditar.setVisible(true);
						ventanaEditar.setLocationRelativeTo(null);
						break;
					case 6:
						ventanaEditar.editarLocalidad(seleccionado.getLocalidad(), seleccionado.getProvincia(), fila);
						ventanaEditar.setVisible(true);
						ventanaEditar.setLocationRelativeTo(null);
						break;
					case 7:
						ventanaEditar.editarCalle(seleccionado.getCalle(), fila);
						ventanaEditar.setVisible(true);
						ventanaEditar.setLocationRelativeTo(null);
						break;
					case 8:
						ventanaEditar.editarAltura(seleccionado.getAltura(), fila);
						ventanaEditar.setVisible(true);
						ventanaEditar.setLocationRelativeTo(null);
						break;
					case 9:
						ventanaEditar.editarGrupoMusica(seleccionado.getGrupoMusical(), fila);
						ventanaEditar.setVisible(true);
						ventanaEditar.setLocationRelativeTo(null);
						break;
					case 10:
						ventanaEditar.editarSignoZ(seleccionado.getSignoZ(), fila);
						ventanaEditar.setVisible(true);
						ventanaEditar.setLocationRelativeTo(null);
						break;
					default:
						break;
				}
				
			}
		}
		
		private void guardarEdicion(ActionEvent g)
		{
			int fila = ventanaEditar.getFilaModificada();
			PersonaDTO a = personasEnTabla.get(fila);
			PersonaDTO nueva = null;
			String datoN = ventanaEditar.getInfoNueva();
			String datoV = ventanaEditar.getInfoAnterior();
			String combo = ventanaEditar.getComboBox();
			String fechaN = "";
			try
			{
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		        fechaN = simpleDateFormat.format(ventanaEditar.getCalendar().getDate());
			}
			catch(Exception  e)
			{
				fechaN = "---";
			}
	        
			switch(ventanaEditar.getDatoModificado())
			{
				case 0:
					if (!datoN.equals("") && !datoN.equals(datoV))
					{
						nueva = new PersonaDTO(a.getIdPersona(), datoN, a.getTelefono(), a.getEmail(), a.getFecha(), a.getTipo(), a.getProvincia(), a.getLocalidad(), a.getCalle(), a.getAltura(), a.getGrupoMusical(), a.getSignoZ());
						this.agenda.borrarPersona(this.personasEnTabla.get(fila));
						this.agenda.agregarPersona(nueva);
					}
					break;
				case 1:
					if (!datoN.equals("") && !datoN.equals(datoV) && validador.check(datoN, "telefono"))
					{
						nueva = new PersonaDTO(a.getIdPersona(), a.getNombre(), datoN, a.getEmail(), a.getFecha(), a.getTipo(), a.getProvincia(), a.getLocalidad(), a.getCalle(), a.getAltura(), a.getGrupoMusical(), a.getSignoZ());
						this.agenda.borrarPersona(this.personasEnTabla.get(fila));
						this.agenda.agregarPersona(nueva);
					}
					break;
				case 2:
					if (!datoN.equals("") && !datoN.equals(datoV) && validador.check(datoN, "email"))
					{
						nueva = new PersonaDTO(a.getIdPersona(), a.getNombre(), a.getTelefono(), datoN, a.getFecha(), a.getTipo(), a.getProvincia(), a.getLocalidad(), a.getCalle(), a.getAltura(), a.getGrupoMusical(), a.getSignoZ());
						this.agenda.borrarPersona(this.personasEnTabla.get(fila));
						this.agenda.agregarPersona(nueva);
					}
					break;
				case 3:
			        if (!fechaN.equals(datoV))
			        {
						nueva = new PersonaDTO(a.getIdPersona(), a.getNombre(), a.getTelefono(), a.getEmail(), fechaN, a.getTipo(), a.getProvincia(), a.getLocalidad(), a.getCalle(), a.getAltura(), a.getGrupoMusical(), a.getSignoZ());
						this.agenda.borrarPersona(this.personasEnTabla.get(fila));
						this.agenda.agregarPersona(nueva);
			        }
			        
			        
					break;
				case 4:
					if (!datoN.equals("---"))
					{
						nueva = new PersonaDTO(a.getIdPersona(), a.getNombre(), a.getTelefono(), a.getEmail(), a.getFecha(), combo, a.getProvincia(), a.getLocalidad(), a.getCalle(), a.getAltura(), a.getGrupoMusical(), a.getSignoZ());
						this.agenda.borrarPersona(this.personasEnTabla.get(fila));
						this.agenda.agregarPersona(nueva);
					}
					break;
				case 5:
					if (!datoN.equals("---"))
					{
						nueva = new PersonaDTO(a.getIdPersona(), a.getNombre(), a.getTelefono(), a.getEmail(), a.getFecha(), a.getTipo(), combo, "---", a.getCalle(), a.getAltura(), a.getGrupoMusical(), a.getSignoZ());
						this.agenda.borrarPersona(this.personasEnTabla.get(fila));
						this.agenda.agregarPersona(nueva);
					}
					break;
				case 6:
					if (!datoN.equals("---"))
					{
						nueva = new PersonaDTO(a.getIdPersona(), a.getNombre(), a.getTelefono(), a.getEmail(), a.getFecha(), a.getTipo(), a.getProvincia(), combo, a.getCalle(), a.getAltura(), a.getGrupoMusical(), a.getSignoZ());
						this.agenda.borrarPersona(this.personasEnTabla.get(fila));
						this.agenda.agregarPersona(nueva);
					}
					break;
				case 7:
					if (!datoN.equals("") && !datoN.equals(datoV))
					{
						nueva = new PersonaDTO(a.getIdPersona(), a.getNombre(), a.getTelefono(), a.getEmail(), a.getFecha(), a.getTipo(), a.getProvincia(), a.getLocalidad(), datoN, a.getAltura(), a.getGrupoMusical(), a.getSignoZ());
						this.agenda.borrarPersona(this.personasEnTabla.get(fila));
						this.agenda.agregarPersona(nueva);
					}
					break;
				case 8:
					if (!datoN.equals("") && !datoN.equals(datoV))
					{
						nueva = new PersonaDTO(a.getIdPersona(), a.getNombre(), a.getTelefono(), a.getEmail(), a.getFecha(), a.getTipo(), a.getProvincia(), a.getLocalidad(), a.getCalle(), datoN, a.getGrupoMusical(), a.getSignoZ());
						this.agenda.borrarPersona(this.personasEnTabla.get(fila));
						this.agenda.agregarPersona(nueva);
					}
					break;
				case 9:
					if (!datoN.equals("---"))
					{
						nueva = new PersonaDTO(a.getIdPersona(), a.getNombre(), a.getTelefono(), a.getEmail(), a.getFecha(), a.getTipo(), a.getProvincia(), a.getLocalidad(), a.getCalle(), a.getAltura(), combo, a.getSignoZ());
						this.agenda.borrarPersona(this.personasEnTabla.get(fila));
						this.agenda.agregarPersona(nueva);
					}
					break;
				case 10:
					if (!datoN.equals("---"))
					{
						nueva = new PersonaDTO(a.getIdPersona(), a.getNombre(), a.getTelefono(), a.getEmail(), a.getFecha(), a.getTipo(), a.getProvincia(), a.getLocalidad(), a.getCalle(), a.getAltura(), a.getGrupoMusical(), combo);
						this.agenda.borrarPersona(this.personasEnTabla.get(fila));
						this.agenda.agregarPersona(nueva);
					}
					break;
				default:
					break;
			}

			this.refrescarTabla();
			ventanaEditar.dispose();
		}
		private void cancelarEdicion(ActionEvent c)
		{
			ventanaEditar.vaciar();
			ventanaEditar.dispose();
		}
		
		public void inicializar()
		{
			this.refrescarTabla();
			this.vista.show();
		}
		
		private void refrescarTabla()
		{
			this.personasEnTabla = agenda.obtenerPersonas();
			Collections.sort(personasEnTabla, new Comparator<PersonaDTO>() {
				@Override
				public int compare(PersonaDTO p1, PersonaDTO p2)
				{
					return (p1.getNombre().toUpperCase()).compareTo(p2.getNombre().toUpperCase());
				}
			});
			this.vista.llenarTabla(this.personasEnTabla);
		}

		@Override
		public void actionPerformed(ActionEvent e) { }
}
