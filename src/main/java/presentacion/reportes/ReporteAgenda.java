package presentacion.reportes;

import java.io.File;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;
import persistencia.conexion.Conexion;

public class ReporteAgenda
{
	private JasperReport reporte;
	private JasperViewer reporteViewer;
	private JasperPrint	reporteLleno;
	private Logger log = Logger.getLogger(ReporteAgenda.class);
	//Recibe la lista de personas para armar el reporte

	Connection conexion = Conexion.getConexion().getSQLConexion();
	
    public ReporteAgenda(String mostrado)
    {
    	//Hardcodeado
		Map<String, Object> parametersMap = new HashMap<String, Object>();
		parametersMap.put("Fecha", new SimpleDateFormat("dd/MM/yyyy").format(new Date()));		
    	try
    	{
    		Connection conexion = Conexion.getConexion().getSQLConexion();
			this.reporte = (JasperReport) JRLoader.loadObjectFromFile( "reportes" + File.separator + "Reporte"+mostrado+".jasper" );
			this.reporteLleno = JasperFillManager.fillReport(reporte, parametersMap, conexion);
    		log.info("Se cargo correctamente el reporte");
		}
    	
		catch( JRException ex ) 
		{
			log.error("Ocurrio un error mientras se cargaba el archivo Reporte", ex);
		}
    }       
    
    public void mostrar()
	{
		this.reporteViewer = new JasperViewer(this.reporteLleno,false);
		this.reporteViewer.setVisible(true);
	}
   
}	