package presentacion.vista;

import java.awt.Color;
import java.awt.Font;
import java.util.Date;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JComboBox;
import com.toedter.calendar.JCalendar;

@SuppressWarnings("serial")
public class Editar extends JFrame {

	private JPanel contentPane;
	private JLabel infoAnterior;
	private JTextField infoNueva;
	private JButton btnCancelar;
	private JButton btnGuardar;
	private JComboBox<String> comboBox;
	private Integer filaModificada;
	private Integer datoModificado;
	private JCalendar calendar;
	private JPanel panel;

	private static Editar INSTANCE;
	
	public static Editar getInstance()
	{
		if(INSTANCE == null)
		{
			INSTANCE = new Editar(); 	
			return new Editar();
		}
		else
			return INSTANCE;
	}

	public Editar()
	{
		setResizable(false);
		setForeground(Color.WHITE);
		setFont(new Font("Arial", Font.PLAIN, 12));
		setTitle("Editar");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 461, 311);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		datoModificado = -1;
		
		infoAnterior = new JLabel("");
		infoAnterior.setHorizontalAlignment(SwingConstants.CENTER);
		infoAnterior.setBounds(88, 45, 257, 36);
		
		infoNueva = new JTextField();
		infoNueva.setHorizontalAlignment(SwingConstants.CENTER);
		infoNueva.setBounds(105, 130, 240, 20);
		
		btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(81, 245, 89, 23);
		contentPane.add(btnCancelar);
		
		btnGuardar = new JButton("Guardar");
		btnGuardar.setBounds(256, 245, 89, 23);
		contentPane.add(btnGuardar);
		
		comboBox = new JComboBox<String>();
		comboBox.setBounds(130, 80, 180, 22);
		
		panel = new JPanel();
		panel.setBounds(96, 71, 245, 172);
		contentPane.add(panel);

		calendar = new JCalendar();
		panel.add(calendar);
		
		@SuppressWarnings("deprecation")
		Date min = new Date(20,0,1);
		calendar.setSelectableDateRange(min, new Date());
		calendar.setDate(new Date());
	}
	
	public Integer getFilaModificada()
	{
		return filaModificada;
	}
	public Integer getDatoModificado()
	{
		return datoModificado;
	}
	public JButton getBtnCancelar() 
	{
		return btnCancelar;
	}
	public JButton getBtnGuardar() 
	{
		return btnGuardar;
	}
	public String getInfoAnterior()
	{
		return infoAnterior.getText();
	}
	public String getInfoNueva()
	{
		return infoNueva.getText();
	}
	public String getComboBox()
	{
		return (String) comboBox.getSelectedItem();
	}
	public JCalendar getCalendar()
	{
		return calendar;
	}
	
	public void editarNombre(String nombre, Integer f)
	{
		this.filaModificada = f;
		datoModificado = 0;
		setTitle("Editar Nombre");
		infoAnterior.setText(nombre);
		contentPane.add(infoAnterior);
		
		infoNueva = new JTextField();
		infoNueva.setHorizontalAlignment(SwingConstants.CENTER);
		infoNueva.setBounds(105, 130, 240, 20);
		contentPane.add(infoNueva);
		infoNueva.setColumns(10);
	}
	
	public void editarTelefono(String telefono, Integer f)
	{
		this.filaModificada = f;
		datoModificado = 1;
		setTitle("Editar Tel�fono");
		infoAnterior.setText(telefono);
		contentPane.add(infoAnterior);
		
		infoNueva = new JTextField();
		infoNueva.setHorizontalAlignment(SwingConstants.CENTER);
		infoNueva.setBounds(105, 130, 240, 20);
		contentPane.add(infoNueva);
		infoNueva.setColumns(10);
	}
	
	public void editarEmail(String Email, Integer f)
	{
		this.filaModificada = f;
		datoModificado = 2;
		setTitle("Editar Email");
		infoAnterior.setText(Email);
		contentPane.add(infoAnterior);
		
		infoNueva = new JTextField();
		infoNueva.setHorizontalAlignment(SwingConstants.CENTER);
		infoNueva.setBounds(105, 130, 240, 20);
		contentPane.add(infoNueva);
		infoNueva.setColumns(10);
	}
	
	public void editarFecha(String fecha, Integer f)
	{
		this.filaModificada = f;
		datoModificado = 3;
		setTitle("Editar Fecha de Cumplea�os");
		infoAnterior.setText(fecha);
		contentPane.add(infoAnterior);

		panel.setVisible(true);
	}
	
	public void editarTipo(String tipo, Integer f)
	{
		this.filaModificada = f;
		datoModificado = 4;
		setTitle("Editar Tipo");
		infoAnterior.setText(tipo);
		contentPane.add(infoAnterior);
		
		comboBox = new JComboBox<String>();
		comboBox.setBounds(130, 80, 180, 22);
		
		String[] tipos = {"Amigo","Trabajo","Familia","Otros"};
		for (String item : tipos)
		{
			if (item.equals(tipo))
				continue;
			comboBox.addItem(item);
		}
		
		contentPane.add(comboBox);
	}
	
	public void editarProvincia(String provincia, Integer f)
	{
		this.filaModificada = f;
		datoModificado = 5;
		setTitle("Editar Provincia");
		infoAnterior.setText(provincia);
		contentPane.add(infoAnterior);
		
		comboBox = new JComboBox<String>();
		comboBox.setBounds(130, 80, 180, 22);
		
		String[] provincias = {"Buenos Aires", "Catamarca", "Cordoba"};
		for (String item : provincias)
		{
			if (item.equals(provincia))
				continue;
			comboBox.addItem(item);
		}
		
		contentPane.add(comboBox);
	}
	
	public void editarLocalidad(String localidad, String provincia, Integer f)
	{
		this.filaModificada = f;
		datoModificado = 6;
		setTitle("Editar Localidad");
		infoAnterior.setText(localidad);
		contentPane.add(infoAnterior);
		
		comboBox = new JComboBox<String>();
		comboBox.setBounds(130, 80, 180, 22);
		String[] localidades1 = {"San Miguel", "Jose C. Paz", "Grand Bourg"};
		String[] localidades2 = {"El Alto", "San Fernando del Valle de Catamarca", "Recreo"};
		String[] localidades3 = {"Jesus Maria", "Colonia Caroya", "Bell Ville"};
		String[] Ldefault = {"---"};
		String[] localidades = provincia.equals("Buenos Aires")?localidades1:provincia.equals("Catamarca")?localidades2:provincia.equals("Cordoba")?localidades3:Ldefault;
		
		for (String item : localidades)
		{
			if (item.equals(localidad))
				continue;
			comboBox.addItem(item);
		}
		contentPane.add(comboBox);
	}
	
	public void editarCalle(String calle, Integer f)
	{
		this.filaModificada = f;
		datoModificado = 7;
		setTitle("Editar Calle");
		infoAnterior.setText(calle);
		contentPane.add(infoAnterior);
		
		infoNueva = new JTextField();
		infoNueva.setHorizontalAlignment(SwingConstants.CENTER);
		infoNueva.setBounds(105, 130, 240, 20);
		contentPane.add(infoNueva);
		infoNueva.setColumns(10);
	}
	
	public void editarAltura(String altura, Integer f)
	{
		this.filaModificada = f;
		datoModificado = 8;
		setTitle("Editar Altura");
		infoAnterior.setText(altura);
		contentPane.add(infoAnterior);
		
		infoNueva = new JTextField();
		infoNueva.setHorizontalAlignment(SwingConstants.CENTER);
		infoNueva.setBounds(105, 130, 240, 20);
		contentPane.add(infoNueva);
		infoNueva.setColumns(10);
	}
	
	public void editarGrupoMusica(String gmusical, Integer f)
	{
		this.filaModificada = f;
		datoModificado = 9;
		setTitle("Editar Grupo Musical Preferido");
		infoAnterior.setText(gmusical);
		contentPane.add(infoAnterior);
		
		comboBox = new JComboBox<String>();
		comboBox.setBounds(130, 80, 180, 22);
		
		String[] gmusicales = {"Air Bag","Los Piojos","Callejeros","Soda Stereo","Otro"};
		for (String item : gmusicales)
		{
			if (item.equals(gmusical))
				continue;
			comboBox.addItem(item);
		}
		
		contentPane.add(comboBox);
	}
	
	public void editarSignoZ(String signoz, Integer f)
	{
		this.filaModificada = f;
		datoModificado = 10;
		setTitle("Editar Grupo Musical Preferido");
		infoAnterior.setText(signoz);
		contentPane.add(infoAnterior);
		
		comboBox = new JComboBox<String>();
		comboBox.setBounds(130, 80, 180, 22);
		
		String[] signosZ = {"Capricornio", "Acuario", "Piscis", "Aries", "Tauro", "G�minis", "C�ncer", "Leo", "Virgo", "Libra", "Escorpio", "Ofiuco", "Sagitario"};
		for (String item : signosZ)
		{
			if (item.equals(signoz))
				continue;
			comboBox.addItem(item);
		}
		
		contentPane.add(comboBox);
	}

	public void vaciar()
	{
		datoModificado = -1;
		infoAnterior.setText("");
		infoNueva.setText("");
		infoNueva.setVisible(false);
		comboBox.removeAllItems();
		comboBox.setVisible(false);
		panel.setVisible(false);
		calendar.setDate(new Date());
	}
}
