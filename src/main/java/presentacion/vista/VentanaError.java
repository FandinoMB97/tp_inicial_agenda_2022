package presentacion.vista;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.Color;

@SuppressWarnings("serial")
public class VentanaError extends JFrame {

	private JPanel contentPane;
	private JLabel textoError;

	public VentanaError() {
		setResizable(false);
		setForeground(Color.WHITE);
		setFont(new Font("Arial", Font.PLAIN, 12));
		setTitle("Contacto no guardado");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 461, 311);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
	}
	public void setTextoError(String texto)
	{
		this.textoError = new JLabel(texto);
		this.textoError.setFont(new Font("Arial", Font.PLAIN, 12));
		this.textoError.setHorizontalAlignment(SwingConstants.CENTER);
		this.textoError.setBounds(0, 29, 434, 190);
		contentPane.add(this.textoError); 
	}
}
