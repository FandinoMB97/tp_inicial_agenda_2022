package presentacion.vista;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import java.awt.Toolkit;
import java.util.Date;
import java.awt.Dialog.ModalExclusionType;
import com.toedter.calendar.JDateChooser;
import javax.swing.JComboBox;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

public class VentanaPersona extends JFrame 
{
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	
	//Contenedores de informacion TextField
	private JTextField _txtNombre;
	private JTextField _txtTelefono;
	private JTextField _txtEmail;
	private JTextField _txtCalle;
	private JTextField _txtAltura;
	//Clendario
	private JDateChooser _fechaNacimiento;
	//ComboBox
	private JComboBox<String> _tipo;
	private JComboBox<String> _provincia;
	private JComboBox<String> _localidad;
	private JComboBox<String> _grupoMusical;
	private JComboBox<String> _signoz;
	
	//Botones
	private JButton _btnAgregarPersona;
	
	
	private static VentanaPersona INSTANCE;
	
	public static VentanaPersona getInstance()
	{
		if(INSTANCE == null)
		{
			INSTANCE = new VentanaPersona(); 	
			return new VentanaPersona();
		}
		else
			return INSTANCE;
	}

	private VentanaPersona() 
	{
		super();
		setModalExclusionType(ModalExclusionType.TOOLKIT_EXCLUDE);
		setTitle("Mi Agenda");
		setIconImage(Toolkit.getDefaultToolkit().getImage(Vista.class.getResource("/presentacion/vista/icono.png")));
		
		//posicion y dimension
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 350, 500);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setForeground(Color.BLACK);
		contentPane.setBackground(Color.DARK_GRAY);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		
		//panel
		JPanel panel = new JPanel();
		panel.setBackground(new Color(32, 178, 170));
		panel.setBounds(10, 11, 307, 439);
		contentPane.add(panel);
		panel.setLayout(null);
		
		//Etiqutas
		JLabel lblNombreYApellido = new JLabel("Nombre y apellido");
		lblNombreYApellido.setBounds(10, 11, 113, 14);
		panel.add(lblNombreYApellido);
		
		JLabel lblTelfono = new JLabel("Telefono");
		lblTelfono.setBounds(10, 52, 113, 14);
		panel.add(lblTelfono);
		
		JLabel lblEmail = new JLabel("Email");
		lblEmail.setBounds(10, 93, 113, 14);
		panel.add(lblEmail);
		
		JLabel lblTipo = new JLabel("Tipo de Contacto");
		lblTipo.setBounds(10, 134, 113, 14);
		panel.add(lblTipo);
		
		JLabel lblFechaN = new JLabel("Fecha de Nacimiento");
		lblFechaN.setBounds(10, 175, 113, 14);
		panel.add(lblFechaN);
		
		JLabel lblProvincia = new JLabel("Provincia");
		lblProvincia.setBounds(10, 216, 113, 14);
		panel.add(lblProvincia);
		
		JLabel lblLocalidad = new JLabel("Localidad");
		lblLocalidad.setBounds(10, 257, 113, 14);
		panel.add(lblLocalidad);
		
		JLabel lblCalle = new JLabel("Calle");
		lblCalle.setBounds(10, 298, 29, 14);
		panel.add(lblCalle);
		
		JLabel lblAltura = new JLabel("Altura");
		lblAltura.setBounds(194, 298, 48, 14);
		panel.add(lblAltura);
		
		JLabel lblGMusical = new JLabel("Grupo Musical");
		lblGMusical.setBounds(10, 339, 113, 14);
		panel.add(lblGMusical);
		
		JLabel lblSignoZ = new JLabel("Signo Zodiacal");
		lblSignoZ.setBounds(10, 380, 113, 14);
		panel.add(lblSignoZ);
		
		
		
		
		//Entrada de Datos tipo text
		_txtNombre = new JTextField();
		_txtNombre.setBounds(133, 8, 164, 20);
		panel.add(_txtNombre);
		_txtNombre.setColumns(10);
		
		_txtTelefono = new JTextField();
		_txtTelefono.setBounds(133, 49, 164, 20);
		panel.add(_txtTelefono);
		_txtTelefono.setColumns(10);
		
		_txtEmail = new JTextField();
		_txtEmail.setColumns(10);
		_txtEmail.setBounds(133, 93, 164, 20);
		panel.add(_txtEmail);
		
		_txtCalle = new JTextField();
		_txtCalle.setColumns(10);
		_txtCalle.setBounds(49, 295, 135, 20);
		panel.add(_txtCalle);
		
		_txtAltura = new JTextField();
		_txtAltura.setColumns(10);
		_txtAltura.setBounds(242, 295, 55, 20);
		panel.add(_txtAltura);
		
		//Panel Fecha
		_fechaNacimiento = new JDateChooser();
		_fechaNacimiento.setBackground(Color.LIGHT_GRAY);
		_fechaNacimiento.getCalendarButton().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		_fechaNacimiento.setBounds(133, 169, 164, 22);
		panel.add(_fechaNacimiento);
		@SuppressWarnings("deprecation")
		Date min = new Date(20,0,1);
		_fechaNacimiento.setSelectableDateRange(min, new Date());
		_fechaNacimiento.setDate(new Date());
		
		//ComboBox
		_tipo = new JComboBox<String>();
		_tipo.setBounds(133, 130, 164, 22);
		panel.add(_tipo);

		_tipo.addItem("Amigo");
		_tipo.addItem("Trabajo");
		_tipo.addItem("Familia");
		_tipo.addItem("Otros");
		
		_provincia = new JComboBox<String>();
		_provincia.setBounds(133, 212, 164, 22);
		panel.add(_provincia);
		
		_provincia.addItem("---");
		_provincia.addItem("Buenos Aires");
		_provincia.addItem("Catamarca");
		_provincia.addItem("Cordoba");
		
		_localidad = new JComboBox<String>();
		_localidad.setBounds(133, 253, 164, 22);
		panel.add(_localidad);
		_localidad.addItem("Null");
		


		_provincia.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0)
			{
				String provincia = (String) getProvincia().getSelectedItem();
				_localidad.removeAllItems();
				if(provincia != null)
					switch( provincia )
					{
						case "Buenos Aires":
							_localidad.addItem("San Miguel");
							_localidad.addItem("Jose C. Paz");
							_localidad.addItem("Grand Bourg");
							break;
						case "Catamarca":
							_localidad.addItem("El Alto");
							_localidad.addItem("San Fernando del Valle de Catamarca");
							_localidad.addItem("Recreo");
							break;
						case "Cordoba":
							_localidad.addItem("Jesus Maria");
							_localidad.addItem("Colonia Caroya");
							_localidad.addItem("Bell Ville");
							break;
						default:
							_localidad.addItem("---");
					}
			
			}
		});
		
		_grupoMusical = new JComboBox<String>();
		_grupoMusical.setBounds(133, 335, 164, 22);
		panel.add(_grupoMusical);
		_grupoMusical.addItem("Air Bag");
		_grupoMusical.addItem("Los Piojos");
		_grupoMusical.addItem("Callejeros");
		_grupoMusical.addItem("Soda Stereo");
		_grupoMusical.addItem("Otro");
		
		_signoz = new JComboBox<String>();
		_signoz.setBounds(133, 376, 164, 22);
		panel.add(_signoz);
		
		_signoz.addItem("Capricornio");
		_signoz.addItem("Acuario");
		_signoz.addItem("Piscis");
		_signoz.addItem("Aries");
		_signoz.addItem("Tauro");
		_signoz.addItem("G�minis");
		_signoz.addItem("C�ncer");
		_signoz.addItem("Leo");
		_signoz.addItem("Virgo");
		_signoz.addItem("Libra");
		_signoz.addItem("Escorpio");
		_signoz.addItem("Ofiuco");
		_signoz.addItem("Sagitario");

		//Botones
		_btnAgregarPersona = new JButton("Agregar");
		_btnAgregarPersona.setBounds(208, 405, 89, 23);
		panel.add(_btnAgregarPersona);
		
	}
	
	public void mostrarVentana()
	{
		_txtNombre.setText(null);
		_txtTelefono.setText(null);
		_txtEmail.setText(null);
		_txtCalle.setText(null);
		_txtAltura.setText(null);
		_fechaNacimiento.setDate(new Date());
		_tipo.setSelectedItem(null);
		_provincia.setSelectedItem(null);
		_localidad.setSelectedItem(null);
		_grupoMusical.setSelectedItem(null);
		_signoz.setSelectedItem(null);
		
		this.setVisible(true);
	}
	
	public JTextField getTxtNombre() 
	{
		return _txtNombre;
	}

	public JTextField getTxtTelefono() 
	{
		return _txtTelefono;
	}
	
	public JTextField getTxtEmail() 
	{
		return _txtEmail;
	}
	
	public JTextField getTxtCalle() 
	{
		return _txtCalle;
	}
	
	public JTextField getTxtAltura() 
	{
		return _txtAltura;
	}

	public JButton getBtnAgregarPersona() 
	{
		return _btnAgregarPersona;
	}
	public JDateChooser getFecha()
	{
		return _fechaNacimiento;
	}
	
	public JComboBox<String> getTipo()
	{
		return _tipo;
	}
	
	public JComboBox<String> getProvincia()
	{
		return _provincia;
	}
	
	public JComboBox<String> getLocalidad()
	{
		return _localidad;
	}
	
	public JComboBox<String> getGrupoMusical()
	{
		return _grupoMusical;
	}
	
	public JComboBox<String> getSignoZ()
	{
		return _signoz;
	}

	public void cerrar()
	{
		this.dispose();
	}
}

