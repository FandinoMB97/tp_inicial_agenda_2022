package presentacion.vista;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import dto.PersonaDTO;

import javax.swing.JButton;

import persistencia.conexion.Conexion;
import java.awt.Toolkit;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import javax.swing.ScrollPaneConstants;

public class Vista
{
	private JFrame frmAgenda;
	private JTable tablaPersonas;
	
	private JButton btnAgregar;
	private JButton btnBorrar;
	private JButton btnReporte;
	private JButton btnEditar;
	private JComboBox<String> reporteBox;
	
	private DefaultTableModel modelPersonas;
	private String[] nombreColumnas = {"Nombre","Telefono","Email","Nacimiento","Tipo","Provincia","Localidad","Calle","Altura","GrupoMusicalFav","S.Zodiacal"};

	public Vista() 
	{
		super();
		initialize();
	}


	private void initialize() 
	{
		frmAgenda = new JFrame();
		frmAgenda.setTitle("Mi Agenda");
		frmAgenda.setBackground(Color.ORANGE);
		frmAgenda.setForeground(Color.ORANGE);
		frmAgenda.setIconImage(Toolkit.getDefaultToolkit().getImage(Vista.class.getResource("/presentacion/vista/icono.png")));
		frmAgenda.setBounds(0, 0, 930, 600);
		frmAgenda.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmAgenda.getContentPane().setLayout(null);
		
		frmAgenda.setResizable(false);	//bloquear el cambio de tamaño de la ventana
		frmAgenda.setLocationRelativeTo(null); //centrar ventana
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.DARK_GRAY);
		panel.setBounds(0, 0, 914, 561);
		frmAgenda.getContentPane().add(panel);
		panel.setLayout(null);
		
		JScrollPane spPersonas = new JScrollPane();
		spPersonas.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		spPersonas.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
		spPersonas.setToolTipText("");
		//spPersonas.setViewportBorder(UIManager.getBorder("Button.border"));
		spPersonas.setBounds(10, 11, 894, 436);
		panel.add(spPersonas);
		
		modelPersonas = new DefaultTableModel(null,nombreColumnas) {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override //Hago que las caldas no puedar ser modificados utilizzando el doble click
			public boolean isCellEditable(int fila, int columna)
			{
				if (columna == nombreColumnas.length)
					return true;
				return false;
			}
		};
		tablaPersonas = new JTable(modelPersonas);
		
		for (int i = 0; i < nombreColumnas.length; i++)
		{
			tablaPersonas.getColumnModel().getColumn(i).setPreferredWidth(100);
			tablaPersonas.getColumnModel().getColumn(i).setResizable(false);
		}
		
		spPersonas.setViewportView(tablaPersonas);
		
		
		btnAgregar = new JButton("Agregar");
		btnAgregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnAgregar.setBounds(10, 484, 89, 44);
		panel.add(btnAgregar);
		
		btnEditar = new JButton("Editar");
		btnEditar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnEditar.setBounds(275, 484, 89, 44);
		panel.add(btnEditar);
		
		btnBorrar = new JButton("Borrar");
		btnBorrar.setBounds(530, 484, 89, 44);
		panel.add(btnBorrar);
		
		btnReporte = new JButton("Reporte");
		btnReporte.setBounds(747, 528, 157, 22);
		panel.add(btnReporte);
		
		reporteBox = new JComboBox<String>();
		reporteBox.setBounds(747, 474, 157, 22);
		panel.add(reporteBox);
		
		reporteBox.addItem("GrupoMusical");
		reporteBox.addItem("SignoZodiacal");
		
	}
	
	public void show()
	{
		this.frmAgenda.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.frmAgenda.addWindowListener(new WindowAdapter() 
		{
			//Confirmacion de cerrar aplicacion (Si / No)
			@Override
		    public void windowClosing(WindowEvent e)
			{
		        int confirm = JOptionPane.showOptionDialog(
		             null, "Est�s seguro que quieres salir de la Agenda?", 
		             "Confirmaci�n", JOptionPane.YES_NO_OPTION,
		             JOptionPane.QUESTION_MESSAGE, null, null, null);
		        if (confirm == 0) {
		        	Conexion.getConexion().cerrarConexion();
		           System.exit(0);
		        }
		    }
		});
		this.frmAgenda.setVisible(true);
	}
	
	public JButton getBtnAgregar() 
	{
		return btnAgregar;
	}

	public JButton getBtnBorrar() 
	{
		return btnBorrar;
	}
	
	public JButton getBtnReporte() 
	{
		return btnReporte;
	}
	
	public JButton getBtnEditar() 
	{
		return btnEditar;
	}
	
	public JComboBox<String> getReporteBox() 
	{
		return reporteBox;
	}
	
	public DefaultTableModel getModelPersonas() 
	{
		return modelPersonas;
	}
	
	public JTable getTablaPersonas()
	{
		return tablaPersonas;
	}

	public String[] getNombreColumnas() 
	{
		return nombreColumnas;
	}


	public void llenarTabla(List<PersonaDTO> personasEnTabla) {
		this.getModelPersonas().setRowCount(0); //Para vaciar la tabla
		this.getModelPersonas().setColumnCount(0);
		this.getModelPersonas().setColumnIdentifiers(this.getNombreColumnas());

		for (PersonaDTO p : personasEnTabla)
		{
			String nombre = p.getNombre();
			String tel = p.getTelefono();
			String email = p.getEmail();
			String fecha = p.getFecha();
			String tipo = p.getTipo();
			String provincia = p.getProvincia();
			String localidad = p.getLocalidad();
			String calle = p.getCalle();
			String altura = p.getAltura();
			String gmusical = p.getGrupoMusical();
			String signoz = p.getSignoZ();
			
			Object[] fila = {nombre, tel, email,fecha,tipo,provincia,localidad,calle,altura,gmusical,signoz};
			this.getModelPersonas().addRow(fila);
		}
		
	}
}
